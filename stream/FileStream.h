#pragma once

#include "OutStream.h"
namespace msl
{
	class FileStream : public OutStream
	{
	private:

		FILE * _file;
	public:
		FileStream(const char* filePath);
		~FileStream();

		FileStream& operator<<(const char *str);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)(FILE* f));
	};
}




