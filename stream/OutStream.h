#pragma once

#include <stdio.h>

namespace msl
{
	class OutStream
	{
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)(FILE* f));
	};


}
void endline(FILE* f);



